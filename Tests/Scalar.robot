*** Settings ***

*** Variables ***
${My_Variable} =  Ahihi
@{list_variable}  = This is Item1  this is Item2  I am Item3

*** Keywords ***

*** Test Cases ***
Variable Demontration
    [Tags]  Current
    ${My_new_Variable} =  set variable  Somthing Else
    log  ${My_new_Variable}

Variable Demontration 2
    [Tags]  List_under_TC
    @{list_va} =  set variable  item 1  item 2  item 3
    log  @{list_va}[0]
    log  @{list_va}[1]
    log  @{list_va}[2]

List Variable
    [Tags]  list
    log  @{list_variable}[0]
    log  @{list_variable}[1]
    log  @{list_variable}[2]
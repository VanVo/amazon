*** Settings ***
Library  Selenium2Library
*** Variables ***
@{URL_AND_BROWSER} =  http://google.com  ff
*** Test Cases ***
TC1 User Can Open Browser
    Begin Web Test  @{URL_AND_BROWSER}
*** Keywords ***
Begin Web Test
    [Arguments]  @{URL_AND_BROWSER}
    Open Browser  @{URL_AND_BROWSER}[0]  @{URL_AND_BROWSER}[1]
    Close Browser
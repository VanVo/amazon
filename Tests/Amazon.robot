*** Settings ***
Resource  ../Resources/Amazon.robot
Resource  ../Resources/Common.robot

Test Setup  Begin Web Test
#Test Teardown  End Web Test

*** Variables ***
${BROSWER} =  gc
${START_URL} =  http://amazon.com
${SEARCH_TERM} =  Ferrari 458

*** Test Cases ***
Logged out user can view a product
    [Tags]  Smoke
    Amazon.Search for Products
    Amazon.Select Product from Search Results

Logged out user can add product to cart
    [Tags]  Smoke2
    Amazon.Search for Products
    Amazon.Select Product from Search Results
    Amazon.Add Product to Cart

Logged out user must sign in to check out
    [Tags]  Smoke
    Amazon.Search for Products
    Amazon.Select Product from Search Results
    Amazon.Add Product to Cart
    Amazon.Begin Checkout

*** Settings ***
Library  Selenium2Library

*** Variables ***
${SIGNIN_HEAD} =  xpath=//h1

*** Keywords ***
Verify Page Loaded
    Page Should Contain Element         ${SIGNIN_HEAD}
    Element Text Should Be              ${SIGNIN_HEAD}  Sign In
